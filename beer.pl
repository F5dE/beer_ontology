:- dynamic inheritances/2.
:- dynamic filtered/1.
:- dynamic alc/2.
:- dynamic originCountry/2.

% type of goddess beer

sub_class(ale, beer).
sub_class(lager, beer).
sub_class(fruit, beer).

sub_class(pilsner, lager).
sub_class(hell, lager).
sub_class(stout, ale).
sub_class(witbier, ale).
sub_class(brownAle, ale).
sub_class(indiaPaleAle, ale).
sub_class(paleAle, ale).
sub_class(cider, fruit).
sub_class(kriek, fruit).

sub_class(dry, cider).
sub_class(semi, cider).
sub_class(sweet, cider).
sub_class(traditionalKriek, kriek).
sub_class(sweetenedKriek, kriek).
sub_class(oudBruinKriek, kriek).

% beer name - beer type 

inheritances(kirin_ichiban, hell).
inheritances(schult_standard, hell).
inheritances(schult_special, hell).
inheritances(sapporo, hell).
inheritances(asahi, hell).
inheritances(birra_moretti, hell).
inheritances(heineken, hell).
inheritances(corona, hell).

inheritances(pilsner_urquel, pilsner).

inheritances(guinness, stout).

inheritances(c, witbier).

inheritances(new_castle, brownAle).

inheritances(iPA, indiaPaleAle).

inheritances(east_west, paleAle).

inheritances(mort_subite, sweetenedKriek).
inheritances(lindemans, sweetenedKriek).
inheritances(kriek_boon, traditionalKriek).
inheritances(timmermnas, traditionalKriek).
inheritances(liefmans, oudBruinKriek).

inheritances(strongbow_dry, dry).
inheritances(somersby_semi_dry, semi).
inheritances(strongbow_gold_apple, sweet).

inheritances(Beer, ParentClass) :- inheritances(Beer, Class), sub_class(Class, ParentClass), !.

% BEER FOR EVERYONE

beer(Beer) :- inheritances(Beer, beer), !.

% lager time, better it to be from Germany (but Japanese is good too)

pilsner(Beer) :- inheritances(Beer, pilsner), !.
hell(Beer) :- inheritances(Beer, hell), !.
lager(Beer) :- inheritances(Beer, lager), !.

% ale is good if its from Belgium

stout(Beer) :- inheritances(Beer, stout), !.
witbier(Beer) :- inheritances(Beer, witbier), !.
brownAle(Beer) :- inheritances(Beer, brownAle), !.
indiaPaleAle(Beer) :- inheritances(Beer, indiaPaleAle), !.
paleAle(Beer) :- inheritances(Beer, paleAle), !.
ale(Beer) :- inheritances(Beer, ale), !.

% true cherry beer for true man

traditionalKriek(Beer) :- inheritances(Beer, traditionalKriek), !.
sweetenedKriek(Beer) :- inheritances(Beer, sweetenedKriek), !.
oudBruinKriek(Beer) :- inheritances(Beer, oudBruinKriek), !.
kriek(Beer) :- inheritances(Beer, kriek), !.

% some cider tonight, my princess

dryCider(Beer) :- inheritances(Beer, dry), !.
semiDryCider(Beer) :- inheritances(Beer, semi), !.
sweetCider(Beer) :- inheritances(Beer, sweet), !.
cider(Beer) :- inheritances(Beer, cider), !.

% bring me some good filtred beer

filtered(Beer) :- inheritances(Beer, lager), !.
filtered(Beer) :- inheritances(Beer, brownAle), !.
filtered(Beer) :- inheritances(Beer, indiaPaleAle), !.
filtered(Beer) :- inheritances(Beer, paleAle), !.
filtered(Beer) :- inheritances(Beer, fruit), !.

% country road - take me home

originCountry(asahi, japan).
originCountry(kirin_ichiban, japan).
originCountry(sapporo, japan).
originCountry(schult_standard, russia).  
originCountry(schult_special, russia).
originCountry(iPA, russia).
originCountry(east_west, usa).
originCountry(birra_moretti, italia).
originCountry(corona, mexico).
originCountry(heineken, germany).
originCountry(pilsner_urquel, czech).
originCountry(hoegaarden, belgium).
originCountry(mort_subite, belgium).
originCountry(lindemans, belgium).
originCountry(kriek_boon, belgium).
originCountry(timmermnas, belgium).
originCountry(liefmans, belgium).
originCountry(guinness, great_britain).
originCountry(new_castle, great_britain).
originCountry(strongbow_dry, great_britain).
originCountry(somersby_semi_dry, great_britain).
originCountry(strongbow_gold_apple, great_britain).
originCountry(_, unknown).

% sweet my life tonight

sweetBeer(Beer) :- inheritances(Beer, kriek), !.
sweetBeer(Beer) :- inheritances(Beer, semi), !.
sweetBeer(Beer) :- inheritances(Beer, sweet), !.

% i dont give a *, who would want to know that, but, anyway, u know that now ^)

fermentation(ale, upFermentation) :- !.
fermentation(lager, downFermentation) :- !.
fermentation(cider, fruitFermentation) :- !.
fermentation(kriek, cherryFermentation) :- !.
fermentation(Beer, Type) :- inheritances(Beer, Class), sub_class(Class, ParentClass), fermentation(ParentClass, Type), !.
fermentation(_, unknown).

% yet another one useless attribute

madeOf(lager, barleyMalt).
madeOf(ale, barleyMalt).
madeOf(cider, fruitsBerries).
madeOf(kriek, cherry).
madeOf(Beer, Type) :- inheritances(Beer, Class), sub_class(Class, ParentClass), madeOf(ParentClass, Type), !.
madeOf(_, unknown).

% higher is better, I suggest going from high to low for better results ^^ 

alc(asahi, 5).
alc(kirin_ichiban, 5).
alc(sapporo, 4.9).
alc(schult_standard, 4.5).
alc(schult_special, 4).
alc(iPA, 5.5).
alc(east_west, 6).
alc(birra_moretti, 4.6).
alc(corona, 4.6).
alc(heineken, 5).
alc(pilsner_urquel, 4.4).
alc(hoegaarden, 4.9).
alc(mort_subite, 4.3).
alc(lindemans, 3.5).
alc(kriek_boon, 4.5).
alc(timmermnas, 4).
alc(liefmans, 6).
alc(guinness, 4.7).
alc(strongbow_dry, 5.3).
alc(somersby_semi_dry, 4.5).
alc(strongbow_gold_apple, 4.5).
alc(new_castle, 4.7).
alc(_, unknown).


% make it preaty *****
showBool(Bool) :- Bool, write("Yes"), !.
showBool(_) :- write("No").

knownPar(unknown) :- !, fail.
knownPar(_) :- !.

% differ that *
% "diferBool(filtered(beer1),filtered(beer2))"
diferBool(Bool1, Bool2) :- Bool1, Bool2, !, fail.
diferBool(Bool1, _) :- Bool1, !.
diferBool(_, Bool2) :- Bool2, !.
diferBool(_, _) :- !, fail.

showFermentationType(upFermentation) :- write("Upper fermentation"), !.
showFermentationType(downFermentation) :- write("Down fermentation"), !.
showFermentationType(fruitFermentation) :- write("Fermantation is based on fruits (apples, pears, etc)"), !.
showFermentationType(cherryFermentation) :- write("Fermentation is based on cherry"), !.
showFermentationType(_) :- write("My lord, I don't have such information! Try another one [ERROR]"), !.

showMadeOf(barleyMalt) :- write("barley malt"), !.
showMadeOf(fruitsBerries) :- write("different fruits and berries"), !.
showMadeOf(cherry) :- write("cherry"), !.

showBeerType(lager) :- write("Lager"), !.
showBeerType(ale) :- write("Ale"), !.
showBeerType(fruit) :- write("Fruit"), !.
showBeerType(pilsner) :- write("Pilsner"), !.
showBeerType(hell) :- write("Münchner Helles"), !.
showBeerType(cider) :- write("Cider"), !.
showBeerType(traditionalKriek) :- write("Traditional kriek lambic"), !.
showBeerType(sweetenedKriek) :- write("Sweetened kriek lambic"), !.
showBeerType(oudBruinKriek) :- write("Oud Bruin kriek lambic"), !.
showBeerType(stout) :- write("Stout"), !.
showBeerType(witbier) :- write("Witbier"), !.
showBeerType(brownAle) :- write("Brown Ale"), !.
showBeerType(paleAle) :- write("Pale Ale"), !.
showBeerType(indiaPaleAle) :- write("India Pale Ale"), !.
showBeerType(dry) :- write("Dry Cider"), !.
showBeerType(semi) :- write("Semi-Dry Cider"), !.
showBeerType(sweet) :- write("Sweet Cider"), !.

showCountry(germany) :- write("Germany"), !.
showCountry(great_britain) :- write("Great Britain"), !.
showCountry(italia) :- write("Italia"), !.
showCountry(czech) :- write("Czech Republic"), !.
showCountry(france) :- write("France"), !.
showCountry(russia) :- write("Russia"), !.
showCountry(japan) :- write("Japan"), !.
showCountry(belgium) :- write("Belgium"), !.
showCountry(mexico) :- write("Mexico"), !.

% attribute 
getAttribute(1, C) :- inheritances(C, X), knownPar(X), showBeerType(X), !.
getAttribute(1, C) :- inheritances(C, unknown), write("I don't know what it is type is!").
getAttribute(2, C) :- fermentation(C, Type), knownPar(Type), showFermentationType(Type), !.
getAttribute(2, C) :- fermentation(C, unknown), write("I don't know what it is fermentation type is!").
getAttribute(3, C) :- madeOf(C, Type), knownPar(Type), write("Made of "), showMadeOf(Type), !.
getAttribute(3, C) :- madeOf(C, unknown), write("I don't know what it is made of!").
getAttribute(4, C) :- alc(C, N), knownPar(N), write("Alcohol: "), write(N), write("%"), !.
getAttribute(4, C) :- alc(C, unknown), write("I don't know what it is Alcohol % is!").
getAttribute(5, C) :- write("Filtred: "), showBool(filtered(C)).
getAttribute(6, C) :- write("Sweet: "), showBool(sweetBeer(C)).
getAttribute(7, C) :- originCountry(C, X), knownPar(X), write("Country: "), showCountry(X), !.
getAttribute(7, C) :- originCountry(C, unknown), write("I don't know what it is country of origin is!").
getAttribute(_, _) :- write("No such attribute, try another one.").

getAttributes(C) :- getAttribute(1, C), nl, getAttribute(2, C), nl, getAttribute(3, C), nl, getAttribute(4, C), nl, getAttribute(5, C), nl, getAttribute(6, C), nl, getAttribute(7, C), !.

showIfDifferent(V1, V2, Attr, C1, C2) :- dif(V1, V2), getAttribute(Attr, C1), write(", "), getAttribute(Attr, C2), nl, !.
showIfDifferent(_, _, _, _, _) :- !.

% solves "attr(obj)" problem
showIfDifferent2(V1, V2, Attr, C1, C2) :- diferBool(V1, V2), getAttribute(Attr, C1), write(", "), getAttribute(Attr, C2), nl, !.
showIfDifferent2(_, _, _, _, _) :- !.

showDiffAttr(C1, C2) :-
    inheritances(C1, V1), inheritances(C2, V2), showIfDifferent(V1, V2, 1, C1, C2),
    fermentation(C1, V3), fermentation(C2, V4), showIfDifferent(V3, V4, 2, C1, C2),
    madeOf(C1, V5), madeOf(C2, V6), showIfDifferent(V5, V6, 3, C1, C2),
    alc(C1, V7), alc(C2, V8), showIfDifferent(V7, V8, 4, C1, C2),
    showIfDifferent2(filtered(C1), filtered(C2), 5, C1, C2),
    showIfDifferent2(sweetBeer(C1), sweetBeer(C2), 6, C1, C2),
    originCountry(C1, V9), originCountry(C2, V10), showIfDifferent(V9, V10, 7, C1, C2), !.

editAttr(C,1,B) :- retract(inheritances(C,_)),asserta(inheritances(C,B)).
editAttr(C,2,B) :- retract(alc(C,_)),asserta(alc(C,B)).
editAttr(C,3,B) :- retract(originCountry(C,_)),asserta(originCountry(C,B)).

% inititation of program for gods
start :- write("Greetings travaler"), nl, init, !.

init :- write("Tell me your demand, my lord! (1 = Get attribute, 2 = Get all attributes, 3 = Show difference, 4 = add element, 5 = change attribute, 6 = exit)"), nl, read(X), stepUp(X), nl.

stepUp(1) :- write("Input object of interest, my lord!"), nl, read(C),
    write("What would you like to know, my lord? (1 = beer type, 2 = fermentation, 3 = made of, 4 = alcohol%, 5 = filtered?, 6 = sweet?, 7 = country of origin)"), nl, read(A), getAttribute(A, C), nl, !, init.

stepUp(2) :- write("Input object of interest, my lord!"), nl, read(C), getAttributes(C), nl, !, init.

stepUp(3) :- write("Input first object of interest, my lord!"), nl, read(C1), write("Input object to compare to, my lord!"), nl, read(C2), showDiffAttr(C1, C2), nl, !, init.

stepUp(4) :- write("Input new object, my lord!"), nl, read(C1), write("Input Beer type, my lord!"), nl, read(C2), asserta(inheritances(C1, C2)), write("Is it filtered, my lord?"), nl, read(C3), (C3 -> asserta(filtered(C1))), write("Input Alcohol %, my lord!"), nl, read(C4), asserta(alc(C1, C4)), write("Input beer country of origin, my lord!"), nl, read(C5), asserta(originCountry(C1, C5)), !, init.

stepUp(5) :- write("Input object of interest, my lord!"), nl, read(C1), write("Input attribute you want to remake, my lord!  (1 = beer type, 2 = alcohol %, 3 = country of origin)"), nl, read(C2), write("Input new attribute value, my lord!"), nl, read(C3), editAttr(C1, C2, C3), !, init.

stepUp(6) :- !.

stepUp(_) :- write("Tell me your demand again, my lord! (1 = Get attribute, 2 = Get all attributes, 3 = Show difference, 4 = exit)"), nl, read(S), stepUp(S), nl, !, init.
